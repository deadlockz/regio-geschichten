# Regio Geschichten

*kleine Geschichten zu Orten und Wege aus der Region*

TESTVERSION

Dies ist eine kleine Webseite, die die Möglichkeiten von Opensource Software,
Web-Applicationen und die Verwendung von gitlab als Community-basiertem
Datenpflege System verdeutlichen soll.

## Orte

https://deadlockz.gitlab.io/regio-geschichten/

## (Rad)Touren

https://deadlockz.gitlab.io/regio-geschichten/touren.html

## Notizen/Todo

Map umbauen mit https://leafletjs.com/examples/quick-start/
